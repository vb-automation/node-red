Node-red (UI) flow for manual reading and inserting values from energy provider and storing them into InfluxDB for further processing.

The separate tarrifs (T1 and T2) are stored with a timestamp equal to the chosen date so gathered values can be inserted retrospecively.

File-based insert would be more appropriate for more than a handful of measurements.
