## Code origin

As the visualization part has been moved to Grafana the original code/nodes from https://github.com/AdamWelchUK/NodeRedEPEverDashboard have been cleaned up.

Following parts come from the original source (above) :

- gathering Epever stats and storing them to Influxdb. 
- Epever configuration UI 

## Own code

Set up Epever system time.
